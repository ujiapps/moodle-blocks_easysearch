@block_easysearch @block_uji @uji
Feature: I should see the corresponding fields dependig on the role

    Background:
        Given the following "users" exist:
            | username | firstname | lastname  | email |
            | user1 | User 1 | User 1 | user1@test.com |
            | user2 | User 2 | User 2 | user2@test.com |
        And the following "roles" exist:
            | name            | shortname |
            | SearchAllFields | searchallfields |
        And I log in as "admin"
        And I set the following system permissions of "SearchAllFields" role:
            | capability                       | permission |
            | block/easysearch:searchallfields | Allow      |
        And the following "system role assigns" exist:
            | user | couse | role |
            | user1 | Acceptance test site | searchallfields |
        And I set the following system permissions of "Authenticated user" role:
            | capability | permission |
            | block/easysearch:myaddinstance | Allow |
        And I am on site homepage
        And I turn editing mode on
        And I add the "Easy Search" block if not present
        And I turn editing mode off
        And I log out

    Scenario: Admin should see the block (she has the moodle/site:config)
        Given I log in as "admin"
        And I am on site homepage

        Then "Easy Search" "block" should exist

    Scenario: A user can add the Easy Search block in the Dashboard
        Given I log in as "user1"
        And I am on homepage
        And I turn editing mode on
        And I add the "Easy Search" block if not present
        And I turn editing mode off

        Then "Easy Search" "block" should exist

    Scenario: A user with searchallfields capability should see all fields
        Given I log in as "user1"
        And I am on homepage
        And I turn editing mode on
        And I add the "Easy Search" block if not present
        And I turn editing mode off

        Then "Easy Search" "block" should exist
        And "input[name=realname]" "css_element" should exist in the "Easy Search" "block"
        And "input[name=username]" "css_element" should exist in the "Easy Search" "block"
        And "input[name=idnumber]" "css_element" should exist in the "Easy Search" "block"
        And "input[name=search]" "css_element" should exist in the "Easy Search" "block"

    Scenario: A user without searchallfields capability should see just course search field
        Given I log in as "user2"
        And I am on site homepage

        Then "Easy Search" "block" should exist
        And "input[name=realname]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=username]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=idnumber]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=search]" "css_element" should exist in the "Easy Search" "block"

    Scenario: Guest user should see the course search field
        Given I log in as "guest"
        And I am on site homepage

        Then "Easy Search" "block" should exist
        And "input[name=realname]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=username]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=idnumber]" "css_element" should not exist in the "Easy Search" "block"
        And "input[name=search]" "css_element" should exist in the "Easy Search" "block"
