<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * EN langpack.
 *
 * @package    block
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 */

$string['pluginname'] = 'Easy Search';
$string['title']  = "Easy Search";
$string['username']   = "Username";
$string['course'] = "Course";
$string['fullname'] = "Name";
$string['easysearch:myaddinstance'] = 'Add a new Easy Search block';
$string['easysearch:searchallfields'] = 'Search all fields';
$string['easysearch:addinstance'] = 'Add a new Easy Search block';
$string['privacy:metadata'] = 'The Easy Search block doesn\'t store personal data.';
$string['usernotfound'] = 'User not found';