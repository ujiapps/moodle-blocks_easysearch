# Easy search block

This block allows a user to search easily for courses and, depending on permissions, for users by different criteria.

## Prerequisites

This plugin is currently maintained for the MOODLE_35_STABLE branch of Moodle, but versions for 3.3 is available in the MOODLE_33_STABLE branch.

## Installation

The code is available at [Bitbucket](https://bitbucket.org/ujiapps/moodle-block_easysearch/).

You can download the code issuing the following command:

```
$ git clone https://bitbucket.org/ujiapps/moodle-block_easysearch.git
```

To install just follow the [Moodle's standard process of installing blocks](https://docs.moodle.org/35/en/Installing_plugins):

* Copy the plugin's root directory to *blocks/easysearch* in your Moodle installation.
* As an administrator visit the *Notifications* page.

## Configuration

No configuration is needed to use the block.

## License

This work is dual-licensed under GPLv3 and EUPLv1.2. You can find more information [here](https://www.uji.es/ujiapps/llicencia).
