<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The block itself.
 *
 * @package    block
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 */

defined('MOODLE_INTERNAL') || die();

class block_easysearch extends block_base {

    public function init() {
        $this->title = get_string('title', 'block_easysearch');
    }
    public function applicable_formats() {
        if (has_capability('moodle/site:config', context_system::instance())) {
            return array('all' => true);
        } else {
            return array('my' => true);
        }
    }
    public function get_content () {
        if (isguestuser()) {
            $showonlycourses = true;
        } else {
            if (!isloggedin() || !has_capability('block/easysearch:myaddinstance', $this->context)) {
                return '';
            }
            $showonlycourses = false;
        }

        if (!isset($this->content) ) {
            $this->content = new StdClass();
        }

        $this->content->footer = '';

        $searchallfields = has_capability('block/easysearch:searchallfields', $this->context);
        $renderable = new block_easysearch\output\easysearch($searchallfields);
        $renderer = $this->page->get_renderer('block_easysearch');
        $this->content->text = $renderer->render($renderable);

        return $this->content;
    }

    public function get_config_for_external()
    {
        return (object) [
            'instance' => get_config('block_easysearch'),
            'plugin' => new stdClass(),
        ];
    }
}

