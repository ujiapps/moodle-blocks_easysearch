<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CA langpack.
 *
 * @package    block
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 */

$string['pluginname'] = "Cerca Ràpida";
$string['title'] = "Cerca ràpida";
$string['fullname'] = "Nom";
$string['course'] = "Curs";
$string['username'] = "Usuari";
$string['easysearch:myaddinstance'] = 'Afegeix un nou bloc de Cerca Ràpida';
$string['easysearch:searchallfields'] = 'Cercar per tots els camps';
$string['easysearch:addinstance'] = 'Afegir un nou block de Cerca Ràpida';
$string['privacy:metadata'] = 'El bloc Cerca Ràpida no enmagatzema cap dada de caràcter personal.';
$string['usernotfound'] = 'Usuari no trobat';