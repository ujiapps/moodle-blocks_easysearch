<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Search by username results page.
 *
 * @package    block
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/weblib.php');

require_login(SITEID, false);

$ctx = context_system::instance();
if (!has_capability('block/easysearch:addinstance', $ctx) || !has_capability('block/easysearch:myaddinstance', $ctx)) {
    print_error("No allowed");
}

$username = optional_param('username', '', PARAM_USERNAME);
$idnumber = optional_param('idnumber', '', PARAM_RAW_TRIMMED);

if ((!$username) && (!$idnumber)) {
    throw new \moodle_exception('usernotfound', 'block_easysearch');
}

$perid = null;
if ($username) {
    try {
        $acad = \local_uji\acad\acad::get_instance();
        $acaduser = $acad->get_user_by_username($username);
        if ($acaduser) {
            $perid = $acaduser->get_perid();
        }
    } catch (\local_uji\acad\exception\user_not_found $e) {
    }
} else {
    $perid = $idnumber;
}

$users = null;
if ($perid) {
    $sql = "SELECT * " .
            "FROM {user} " .
            "WHERE idnumber=:idnumber and " .
            "      deleted=:deleted " .
            "ORDER BY lastaccess DESC";

    $users = $DB->get_records_sql($sql, array('idnumber' => $perid, 'deleted' => 0));
}
if (!$users && $username) {
    $users = \core_user::get_user_by_username($username);
    if (!$users) {
        throw new \moodle_exception('usernotfound', 'block_easysearch');
    }
    $users = [$users];
}
if (!$users) {
    throw new \moodle_exception('usernotfound', 'block_easysearch');
}

$PAGE->set_url('/blocks/easysearch/username_page.php', array('username' => $username));
$PAGE->set_title(get_string('title', 'block_easysearch'));
$PAGE->set_heading(get_string('title', 'block_easysearch'));
$PAGE->navbar->add(get_string('title', 'block_easysearch'));
echo $OUTPUT->header();

$strbox = <<<_EOF
Esta es una página de búsqueda hecha a medida. Tenemos gente que aparece varias veces
y esto facilita su localización y tratamiento
_EOF;

$OUTPUT->box($strbox);

$table = new html_table();
$table->head[] = 'Activo';
$table->head[] = 'Nombre';
$table->head[] = 'Auth';
$table->head[] = 'Username';
$table->head[] = 'Idnumber/Perid';
$table->head[] = 'Email';
$table->head[] = 'Ciudad';
$table->head[] = 'País';
$table->head[] = 'Último acceso';

$table->align = array("center", "left", "center", "center", "center", "center", "center", "left");

foreach ($users as $u) {

    try {
        \core_user::require_active_user($u, false, false);
        $activo = "Sí";
    } catch (\moodle_exception $e) {
        $activo = "No";
    }

    $name = \html_writer::link(
        new \moodle_url('/user/view.php', ['id' => $u->id]),
        $u->firstname . ' ' . $u->lastname
    );

    $table->data[] = [
        $activo,
        $name,
        get_string('pluginname', 'auth_' . $u->auth),
        $u->username,
        $u->idnumber,
        $u->email,
        $u->city,
        $u->country,
        userdate($u->lastaccess)
    ];
}

echo html_writer::table($table);
echo $OUTPUT->footer();
