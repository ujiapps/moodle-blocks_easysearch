<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ES langpack.
 *
 * @package    block
 * @copyright  2017 Universitat Jaume I (http://www.uji.es/)
 * @license    https://www.uji.es/ujiapps/llicencia Dual licensed under GNU GPLv3 and EUPLv1.2
 * @author     Juan Segarra Montesinos <juan.segarra@uji.es>
 */

$string['pluginname'] = 'Búsqueda Rápida';
$string['title'] = "Búsqueda Rápida";
$string['username']   = "Usuario";
$string['course'] = "Curso";
$string['fullname' ] = "Nombre";
$string['easysearch:myaddinstance'] = 'Añadir un bloque de Búsqueda Rápida';
$string['easysearch:searchallfields'] = 'Buscar por todos los campos';
$string['easysearch:addinstance'] = 'Añadir un nuevo bloque de Búsqueda Rápida';
$string['privacy:metadata'] = 'El bloque Búsqueda Rápida no almacena datos de carácter personal.';
$string['usernotfound'] = 'Usuario no encontrado';